<?php
add_action( 'wp_enqueue_scripts', 'teatronika_enqueue_styles' );
function teatronika_enqueue_styles() {
 
    $parent_style = 'parent-style';
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );

	wp_enqueue_script( 'isotope', 'https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js', array('jquery'), '3.0.6');
}

/* login CSS style */
function teatronika_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/login.css' );
}
add_action( 'login_enqueue_scripts', 'teatronika_login_stylesheet' );

/**
 * redirect after login and logout
 */
add_filter('login_redirect', 'teatronika_default_page', 10, 3);
add_filter('logout_redirect', 'teatronika_default_page', 10, 3);
function teatronika_default_page($redirect, $request, $user) {
	return ( home_url('archivo-de-guiones') );
}

// CPT UI radio post type export
function teatronika_register_my_cpts_guion() {

	/**
	 * Post Type: Radios.
	 */

	$labels = array(
		"name" => __( "Guión", "teatronika" ),
		"singular_name" => __( "Guión", "teatronika" ),
	);

	register_taxonomy( 'guion_genre', 'guion', array(
		'label'		   => 'Género',
        'rewrite'      => array( 'slug' => 'guion_genre' ),
        'capabilities' => array(
			'assign_terms' => 'edit_guion',
			'edit_terms' => 'publish_guion'
		)
    ) );

	register_taxonomy( 'guion_tag', 'guion', array(
		'label'		   => 'Etiquetas',
        'rewrite'      => array( 'slug' => 'guion_tag' ),
        'capabilities' => array(
			'assign_terms' => 'edit_guion',
			'edit_terms' => 'publish_guion'
		)
    ) );

	$args = array(
		"label" => __( "Guión", "teatronika" ),
		"labels" => $labels,
		"description" => "Guión",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "guion", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-clipboard",
		"supports" => array( "title", "author", "thumbnail" ),
		"taxonomies" => array( "guion_tag", "guion_genre" ),
	);

	register_post_type( "guion", $args );
}

add_action( 'init', 'teatronika_register_my_cpts_guion' );

// ACF field group export
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_guion',
		'title' => 'Guión',
		'fields' => array (
			array (
				'key' => 'field_sinopsis_1',
				'label' => 'Sinopsis',
				'name' => 'sinopsis',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_personajes_1',
				'label' => 'Personajes',
				'name' => 'personajes',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_nombre_1',
				'label' => 'Nombre autor',
				'name' => 'nombre',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_apellidos_1',
				'label' => 'Apellidos autor',
				'name' => 'apellido',
				'type' => 'text',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'formatting' => 'none',
				'maxlength' => '',
			),
			array (
				'key' => 'field_edicion_1',
				'label' => 'Edición',
				'name' => 'edicion',
				'type' => 'radio',
				'choices' => array (
					'2015' => '2015',
					'2018' => '2018',
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_robots_1',
				'label' => 'Número de robots',
				'name' => 'robots',
				'type' => 'number',
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'min' => 0,
				'step' => 1,
			),
			array (
				'key' => 'field_pdf_1',
				'label' => 'PDF',
				'name' => 'pdf',
				'type' => 'file',
				'required' => 0,
				'save_format' => 'object',
				'library' => 'uploadedTo',
				'mime_types' => 'pdf',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'guion',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));

	acf_add_local_field_group(array(
		'key' => 'group_5f8851c2dd5d2',
		'title' => 'User info',
		'fields' => array(
			array(
				'key' => 'field_5f88523989832',
				'label' => 'Intenciones con nosotros',
				'name' => 'intenciones',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'acceso' => 'Tener acceso a la biblioteca',
					'futuroautor' => 'Me gustaría ser autor de un guión',
					'autor' => 'Soy autor de un guión',
				),
				'allow_null' => 1,
				'other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
				'return_format' => 'value',
				'save_other_choice' => 0,
			),
			array(
				'key' => 'field_5f8852cb1e636',
				'label' => 'Temas preferidos',
				'name' => 'preferencias',
				'type' => 'checkbox',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array(
					'artes' => 'Artes escénicas',
					'ciencia' => 'Ciencia ficción',
					'robotica' => 'Robótica',
					'Guiones' => 'Guiones',
					'ia' => 'Inteligencia artificial',
				),
				'allow_custom' => 0,
				'default_value' => array(
				),
				'layout' => 'vertical',
				'toggle' => 0,
				'return_format' => 'value',
				'save_custom' => 0,
			),
			array(
				'key' => 'field_5f8851e77da57',
				'label' => 'Aceptar info',
				'name' => 'aceptar',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => 'Aceptas recibir noticias de Teatronika (no mas de 5 anuales).',
				'default_value' => 0,
				'ui' => 0,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'user_form',
					'operator' => '==',
					'value' => 'all',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => true,
		'description' => '',
	));
}

/* add capability file upload to users */
function teatronika_add_cap_upload() {
	// admin
	$role = get_role( 'administrator' );
    $role->add_cap( 'create_guion' ); 
    $role->add_cap( 'edit_guion' ); 
    $role->add_cap( 'edit_published_guion' ); 
    $role->add_cap( 'edit_private_guion' ); 
    $role->add_cap( 'edit_others_guion' ); 
    $role->add_cap( 'publish_guion' ); 
    $role->add_cap( 'read_private_guion' ); 
    $role->add_cap( 'delete_private_guion' ); 
    $role->add_cap( 'delete_published_guion' ); 
    $role->add_cap( 'delete_others_guion' );
}
add_action( 'init', 'teatronika_add_cap_upload' );

/*
 * Add custom column date to guion backend table and make sortable
 */
function teatronika_user_columns($columns) {
    $columns['intencion'] = 'Intenciones';
    $columns['preferencia'] = 'Preferencias';
    $columns['aceptar'] = 'Acepta noticias';
    return $columns;
}
add_filter('manage_users_columns', 'teatronika_user_columns');

function teatronika_show_user_columns($value, $column_name, $user_id) {
    $post_id = "user_".$user_id;

    if ( 'aceptar' == $column_name ) {
    	$aceptar = get_field('aceptar', $post_id);
    	return ($aceptar == '1') ? 'Sí' : 'No';
    }
    else if ( 'intencion' == $column_name ) {
    	return get_field('intenciones', $post_id);
    }
    else if ( 'preferencia' == $column_name ) {
    	return implode(',', get_field('preferencias', $post_id));
    }
    return $value;
}
add_action('manage_users_custom_column',  'teatronika_show_user_columns', 10, 3);