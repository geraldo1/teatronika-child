<?php
/**
 * Template Name: Script
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

$args = array(
	'post_type' => 'guion',
	'post_status' => 'publish',
	'posts_per_page' => '1000',
	'order' => 'ASC',
	'orderby' => 'ID'
);

get_header(); ?>

<!-- Modal window for unregistered users -->
<div id="modalWindow" class="modal">
	<div class="modal-content">
		<div class="modal-header">
			<span class="close">&times;</span>
			<h2>Login necesario</h2>
		</div>
		<div class="modal-body">
			<p>Para ver el guión completo, tienes que <a href="<?php echo esc_url( wp_login_url( home_url( add_query_arg( array(), $wp->request ) ) ) ); ?>">registrarte</a> primero.</p>
		</div>
		<div class="modal-footer">
		    <button id="btn-close">Cerrar</button>
		</div>
    </div>
</div>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<h1>Archivo de guiones</h1>

			<div class="sort">Ordenar por: 
				<button value="title">Título</button>
				<button value="autor">Autor</button>
				<button value="edition">Edición</button>
				<button value="genre">Genre</button>
			</div>

			<div class="filter">Filtrar por edición: 
				<button value="*">Todos</button>
				<button value="2015">2015</button>
				<button value="2018">2018</button>
			</div>

			<?php
				$genres = get_terms( 'guion_genre', array( 'hide_empty' => false ) );
				$tags = get_terms( 'guion_tag' );
			?>

			<div class="filter">Filtrar por genre: 
				<button value="*">Todos</button>
				<?php foreach ($genres as $genre) { ?>
					<button class="<?php echo $genre->slug; ?>" value="<?php echo $genre->slug; ?>"><?php echo $genre->name; ?></button>
				<?php } ?>
			</div>

			<div>
				Filtrar por: <input type="text" id="filterInput">
			</div>

			<div class="grid">

			<?php
				$my_query = new WP_Query($args);

				if ( $my_query->have_posts() ) {

					$data = array();

					while ($my_query->have_posts()) {

						$my_query->the_post();

						$sinopsis = get_post_meta(get_the_ID(), 'sinopsis', true);
						$personajes = get_post_meta(get_the_ID(), 'personajes', true);
						$nombre = get_post_meta(get_the_ID(), 'nombre', true);
						$apellido = get_post_meta(get_the_ID(), 'apellido', true);
						$edicion = get_post_meta(get_the_ID(), 'edicion', true);
						$robots = get_post_meta(get_the_ID(), 'robots', true);
						$pdf = wp_get_attachment_url(get_post_meta(get_the_ID(), 'pdf', true));
						$genres = get_the_terms( get_the_ID(), 'guion_genre' );
						$tags = get_the_terms( get_the_ID(), 'guion_tag' );
						
						echo "<div class='box ".$edicion." ".$genres[0]->slug."'>";
						//echo "<div class='box ".$edicion."'>";
						the_post_thumbnail( 'medium' ); 
						echo "<div class='title'>".get_the_title()."</div>";
						echo "<p class='autor'>Autor: ".$nombre." ".$apellido."</p>";
						echo "<p class='edition'>Edición: ".$edicion."</p>";
						echo "<p class='robots'>Robots: ".$robots."</p>";
						echo "<p class='genre'>Genre: ".$genres[0]->name."</p>";
						//echo "<p class='tags'>Tags: ".$tags."</p><br />";

						$terms = get_the_terms( get_the_ID(), 'guion_tag' );
						if ( $terms && ! is_wp_error( $terms ) ) {
						    $tags = array();
						    foreach ( $terms as $term ) {
						        $tags[] = $term->name;
						    }
						    $on_tags = join( ", ", $tags );
						 
					    	echo '<p class="tags">';
						    printf( esc_html__( 'Tags: %s', 'teatronika' ), esc_html( $on_tags ) );
						    echo '</p>';
						}

						echo "<br><p class='sinopsis'><label>Sinopsis</label>: ".$sinopsis."</p>";
						echo "<p class='personajes'><label>Personajes</label>: ".$personajes."</p>";

						if ($pdf) {
							//if (current_user_can('subscriber') || current_user_can('administrator')) {
							if ( is_user_logged_in() ) {

								echo "<br><p><a class='btn-pdf pdf' href='".$pdf."'>Download PDF</a></p>";
							}
							else {
								echo "<br><p><a class='btn-login pdf' href='#'>Download PDF</a></p>";
							}
						}
						echo "</div>";
					}
				}
			?>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<script>
	jQuery(document).ready(function($) {
	
		var $grid = jQuery('.grid').isotope({
			itemSelector: '.box',
			//layoutMode: 'fitRows',
			layoutMode: 'masonry',
		    masonry: {
				columnWidth: 270
			},
			fitRows: {
				gutter: 20
			},
			getSortData: {
			    title: '.title',
			    autor: '.autor',
			    edition: '.edition',
			    genre: '.genre',
			}
		});

		/*var $items = $grid.find('.box');
		$grid.addClass('is-showing-items').isotope( 'revealItemElements', $items );*/

		jQuery(".sort button").click(function() {
			$grid.isotope({ sortBy: jQuery(this).val() })
		});

		jQuery(".filter button").click(function() {
			var value = jQuery(this).val();
			if (value !== "*") value = "."+value;
			$grid.isotope({ filter: value })
		});

		jQuery(".box").click(function() {
			var active = false;
			if (jQuery(this).hasClass("big")) active = true;

			jQuery(".box").removeClass("big");
			if (!active) jQuery(this).addClass("big");
			else jQuery(this).removeClass("big");

			$grid.isotope('layout');
		});

		jQuery('#filterInput').on('keyup', function(event) {
			var filterValue = "*";
			var searchText = jQuery(this).val();

			if (searchText.length > 1) {
			   	jQuery('.filter button.is-checked').removeClass('is-checked');

				filterValue = function() {
					var autor = jQuery(this).find('.autor').text(),
						edition = jQuery(this).find('.edition').text(),
						genre = jQuery(this).find('.genre').text(),
						sinopsis = jQuery(this).find('.sinopsis').text(),
						personajes = jQuery(this).find('.personajes').text(),
						text = autor+" "+edition+" "+genre+" "+sinopsis+" "+personajes,
						re = new RegExp(searchText, 'gi');
					console.log(text);
					return text.match(re);
				}			
			} else {
			   	jQuery('.filter button.all').addClass('is-checked');
			}

			$grid.isotope({ filter: filterValue });
		});

		// PDF button
		jQuery(".btn-pdf").click(function(event) {
			event.stopPropagation();
		});

		// Modal window
		var modal = document.getElementById("modalWindow");

		// pdf button for not logged users
		jQuery(".btn-login").click(function(event) {
			modal.style.display = "block";
			event.stopPropagation();
		});

		// modal close button
		jQuery(".close").click(function() {
			modal.style.display = "none";
		});

		jQuery("#btn-close").click(function() {
			modal.style.display = "none";
		});

		// click outside modal window -> close modal
		window.onclick = function(event) {
			if (event.target == modal) {
				modal.style.display = "none";
			}
		} 
	});
</script>

<?php
get_footer();
