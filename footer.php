<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-wp-content/uploads/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">

			<div class="wrap">

				<p>Colaboran UPF, Radio 3, FECYT, Institut de Cultura de Barcelona</p>
				
				<table>
					<tr>
						<td>
							<a href="https://www.upf.edu/"><img class="size-medium wp-image-25 aligncenter" src="https://teatronika.org/wp-content/uploads/2018/06/logoupf.png" alt="" width="300" height="101" /></a>
						</td>

						<td>
							<a href="https://www.rtve.es/radio/radio3/"><img class="size-medium wp-image-61 aligncenter" src="https://teatronika.org/wp-content/uploads/2018/06/radio3.png" alt="" width="300" height="79" /></a>
						</td>

						<td>
							<a href="https://www.fecyt.es/"><img class="aligncenter wp-image-264 size-full" src="https://teatronika.org/wp-content/uploads/2019/04/fecyt-web.jpg" alt="" width="1252" height="204" /></a>
						</td>

						<td>
							<a href="https://www.barcelona.cat/barcelonacultura/ca/icub"><img class="size-medium wp-image-63 aligncenter" src="https://teatronika.org/wp-content/uploads/2018/06/icub.jpg" alt="" width="300" height="202" /></a>
						</td>
					</tr>
				</table>
			</div><!-- .wrap -->

		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
